import { observable , action} from 'mobx'

class manualStore {
    @observable number = 0

    @action minus = () => {
        this.number -= 1
    }

    @action plus = () => {
        this.number += 1
    }
}

export default new manualStore()