import './style.scss'
import Home from '../Home'
import Page from '../Page'
import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import { Provider } from 'mobx-react'
import manualStore from '../../store/manualStore'

export default class Site extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() { }

    render() {
        return (
            <Provider manualStore={manualStore}>
                <BrowserRouter>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/page' component={Page} />
                </BrowserRouter>
            </Provider>
        )
    }
}