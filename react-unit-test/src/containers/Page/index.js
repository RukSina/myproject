// import React, { Component } from 'react'
// import { inject, observer } from 'mobx-react';

// @inject(['manualStore'])
// @observer
// export default class componentName extends Component {
//     render() {
//         return (
//             <div className="app">
//                 <h1>Page</h1>
//                 <h1>{this.props.manualStore.number}</h1>
//             </div>
//         )
//     }
// }

import React from 'react';
function App() {
  return (
    <div className="app">
      <h1>Hello React App</h1>
    </div>
  );
}
export default App;
