import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react';

import * as Styled from './styled-components'

// import Image from '../../asserts/googlelogo_color_272x92dp.png'

@inject(['manualStore'])
@observer
export default class Home extends Component {
    constructor(props) {
        super(props)

    }

    componentDidMount() {
    }

    render() {
        return (
            <div>
                <Styled.HeadingText>manual-boilerplate</Styled.HeadingText>
                <h1>manual-boilerplate</h1>
                {/* <img src={Image} /> */}
                <Styled.Number>{this.props.manualStore.number}</Styled.Number>
                <button onClick={() => this.props.manualStore.minus()}>minus</button>
                <button onClick={() => this.props.manualStore.plus()}>plus</button>
                <p><Link to='/page'>go to page</Link></p>
            </div>
        )
    }
}