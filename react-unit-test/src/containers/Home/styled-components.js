import styled from 'styled-components';

export const HeadingText = styled.h1`
    text-align: right;
`;

export const Number = styled.p`
    font-size: 6em;
`;