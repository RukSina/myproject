import React from 'react';
import { mount, shallow, render } from 'enzyme';

import Home from './index'
import { Provider } from 'mobx-react';
import manualStore from '../../store/manualStore'

const defaultNumber = 0

let _manualStore = manualStore

let mockHistory = { push: jest.fn() };

global.window.alert = jest.fn()
global.window.confirm = jest.fn()

describe('<Home/>', () => {
  // beforeEach(() => {
  //   // extend(_manualStore, { defaultNumber: 0 })
  // //   mockHistory.push.mockReset();
  // //   global.window.alert.mockReset()
  // //   global.window.confirm.mockReset()
  // });

  it('should click plus then value plus 1', () => {
    // extend(_manualStore, { number: 0 })
    const wrapper = mount(
      <Provider manualStore={_manualStore}>
        <Home history={mockHistory} />
      </Provider>
    );
    wrapper.find('button').simulate('click');
    expect(_manualStore.defaultNumber).toEqual(1);
  });

//   it('should redirect correctly cause does not have selectedSkuStore', () => {

//     const wrapper = mount(
//         <Provider  manualStore={_manualStore} >
//             <Home history={mockHistory} />
//         </Provider>
//     );
//     expect(mockHistory.push).toHaveBeenCalledTimes(1)
// });
})