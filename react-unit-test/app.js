const express = require('express')
const app = express()
const port = 8080

const path = require('path')
const fs = require('fs')

const path_html = path.join(__dirname, 'html')
const path_index = path.join(__dirname, 'html', 'index.html')

app.get('/', (req, res) => {
    console.log('home')
    const page = meta('default')
    res.send(page)
})

app.use(express.static(path_html))

app.get('*', (req, res) => {
    console.log('star')
    const page = meta('default')
    res.send(page)
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

const list = [
    {
        url: 'default',
        title: 'Home',
        description: 'Here is default description',
        keywords: 'Here is default keyword'
    }
]

meta = url => {
    const index = path_index
    const selected = list.find(i => i.url === url)
    let tag = fs.readFileSync(index, 'utf8')
    console.log(typeof tag)
    tag = tag.replace(/\$title/g, selected.title)
    tag = tag.replace(/\$description/g, selected.description)
    tag = tag.replace(/\$keywords/g, selected.keywords)
    tag = tag.replace(
        /\$url/g,
        `http://localhost:8080/${url === 'default' ? '' : selected.url}`
    )
    return tag
}