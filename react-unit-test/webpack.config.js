const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    output: {
        path: __dirname + '/html',
        publicPath: '/',
        filename: '[hash].js',
        chunkFilename: '[chunkhash].js'
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            cacheGroups: {
                default: false,
                react: {
                    test: /[\\/]node_modules[\\/](react|react-dom|react-fast-compare|react-helmet|react-hot-loader|react-is|react-lifecycles-compat|react-moment|react-router|react-router-dom|react-side-effect|react-test-renderer|react-transition-group)[\\/]/,
                    name: 'react',
                    chunks: 'all'
                },
                materialui: {
                    test: /[\\/]node_modules[\\/]@material-ui[\\/]/,
                    name: 'material-ui',
                    chunks: 'all'
                },
                styled: {
                    test: /[\\/]node_modules[\\/]styled-components[\\/]/,
                    name: 'styled-components',
                    chunks: 'all'
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: process.env.NODE_ENV === 'development',
                            reloadAll: true,
                        },
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: './images/[hash].[ext]',
                        },
                    },
                ],
            },
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './public/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: '[hash].css'
        }),
    ]
}