import React from "react";
import ReactDOM from "react-dom";
import Site from "./containers/Site/";

ReactDOM.render(<Site />, document.getElementById("app"));