import styled from "styled-components";
import { Grid, Typography } from "@material-ui/core";
import BgImage from "../../assets/login-background.svg"

export const LoginWrapper = styled(Grid)`
    background-color: #cbfcda;
    @media (min-width: 600px) {
    }
    @media (min-width: 960px) {
    }
    @media (min-width: 1280px) {
        background-color: inherit;
    }
`;

export const CustomLoginWrapper = styled.div`
    width: 100%;
    form {
        margin: 10px;
        padding: 40px 20px;
        border: 1px solid #cbfcda;
        border-radius: 10px;
        background-color: #ffffff;
    }
    @media (min-width: 600px) {
        width: 60%;
    }
    @media (min-width: 960px) {
        width: 40%;
    }
    @media (min-width: 1280px) {
        width: 55%;
        margin-left: -20%;
        form {
            border: inherit;
            background-color: inherit;
        }
    }
`;

export const CustomLoginHeader = styled(Typography)`
    text-align: center;
    @media (min-width: 600px) {
    }
    @media (min-width: 960px) {
    }
    @media (min-width: 1280px) {
        text-align: left;
    }
`;

export const CustomGridColorWithBgImage = styled(Grid)`
    background: linear-gradient(138deg, #cbfcda 0%, rgba(82,62,195,1) 45%, rgba(48,38,105,1) 100%);
    position: relative;
    div.bg {
        width: 100%;
        height: 100%;
        position: absolute;
        background: url(${BgImage});
        background-repeat: no-repeat;
        background-size: 2179px;
        background-position: -640px -180px;
    }
`;

export const CustomTermsWrapper = styled.div`
    text-align: center;
    @media (min-width: 600px) {
    }
    @media (min-width: 960px) {
    }
    @media (min-width: 1280px) {
        text-align: left;
    }
`;

export const CustomImage = styled.img`
    position: absolute;
    top: 10%;
    left: 10%;
`;