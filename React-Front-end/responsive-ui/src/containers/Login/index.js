import { Grid, Button, Hidden, Typography, Checkbox, FormControlLabel } from "@material-ui/core"
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import React, { Component } from "react"
import { LoginWrapper, CustomLoginWrapper, CustomLoginHeader, CustomGridColorWithBgImage, CustomTermsWrapper, CustomImage } from "./styled-components"
import Logo from "../../assets/login-logo.svg"


class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            // remember: localStorage.getItem("remember") == "true",
            loading: false,
          
        }
    }

    componentDidMount() {
    }


    render() {
        return (
            <LoginWrapper className="h-100" container>
                <Grid container item xs={12} lg={6} alignItems="center" justifyContent="center" direction="column">
                    <CustomLoginWrapper>
                        <Hidden mdDown>
                            <CustomGridColorWithBgImage container item md={12} lg={6} alignItems="center" justifyContent="center" direction="column">
                                <div className="bg"></div>
                                <CustomImage className="animate__animated animate__fadeInLeft" src={Logo} />
                                <div>
                                    <Typography className="text-white animate__animated animate__fadeInLeft" variant="h3"><b>Experience new things inside</b></Typography>
                                    <Typography className="text-white animate__animated animate__fadeInLeft" variant="h5">Manage all your display in one place</Typography>
                                </div>
                            </CustomGridColorWithBgImage>
                        </Hidden>
                        <ValidatorForm
                            ref="form"
                            onSubmit={() => this.handleSubmit()}
                            onError={(error) => console.error(error)}>
                            <CustomLoginHeader className="mb-20" variant="h4"><b>Sign In</b></CustomLoginHeader>
                            {/* {
                                localStorage.getItem("username") == "siwakorn@eibiz.co.th" &&
                                <div className="mb-20">
                                    <button onClick={() => this.setState({ username: "chaiwat@eibiz.co.th", password: "Eibiz1234*" })}>System Admin</button>
                                    &nbsp;
                                    <button onClick={() => this.setState({ username: "siwakorn@eibiz.co.th", password: "Eibiz1234*" })}>Company Admin</button>
                                    &nbsp;
                                    <button onClick={() => this.setState({ username: "terror.tyro@gmail.com", password: "Eibiz1234*" })}>Designer</button>
                                </div>
                            } */}
                            <TextValidator
                                label="Email"
                                name="login_email"
                                onChange={(e) => this.handleChange("username", e.target.value)}
                                value={this.state.username}
                                validators={["required", "isEmail"]}
                                errorMessages={["Email is a required field", "Email is not valid"]}
                                variant="outlined"
                                autoFocus={false}
                                size="small"
                                fullWidth={true}
                            />
                            <TextValidator
                                className="mt-20"
                                label="Password"
                                name="login_password"
                                type="password"
                                onChange={(e) => this.handleChange("password", e.target.value)}
                                value={this.state.password}
                                validators={["required"]}
                                errorMessages={["Password is a required field"]}
                                variant="outlined"
                                size="small"
                                fullWidth={true}
                            />
                            <Grid container>
                                <Grid container item xs={6} alignItems="center" justifyContent="flex-start">
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                color="primary"
                                                name="login_rememberme"
                                                checked={this.state.remember}
                                                onChange={() => this.setState({ remember: !this.state.remember })}
                                                value={this.state.remember}
                                            />
                                        }
                                        label="Remember me"
                                    />
                                </Grid>
                                <Grid container item xs={6} alignItems="center" justifyContent="flex-end">
                                    <Typography className="pointer" color="primary" name="login_forgot_password" onClick={() => this.props.history.push("/forgot-password")}>Forgot Password?</Typography>
                                </Grid>
                            </Grid>
                            <Button className="mt-20 mb-20" type="submit" name="login_button" color="primary" variant="contained" fullWidth>
                                {this.state.loading ? <React.Fragment>&nbsp;<i className="fa fa-spinner fa-pulse fa-fw" />&nbsp;</React.Fragment> : "Login"}
                            </Button>
                            <Typography color="error">{this.state.errorMessage ? this.state.errorMessage : "\u00A0"}</Typography>
                            <CustomTermsWrapper className="mt-20">
                                <Typography>By signin up, you agree to our</Typography>
                                <Typography className="pointer" color="primary" name="login_tc" component="span" onClick={() => this.setState({ modalTC: true })}>Terms and Conditions</Typography>
                            </CustomTermsWrapper>
                        </ValidatorForm>
                    </CustomLoginWrapper>
                </Grid>
            </LoginWrapper>
        )
    }
}
export default Login