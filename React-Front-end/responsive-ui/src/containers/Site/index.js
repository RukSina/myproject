import React, { Component } from "react"
import "./style.scss";
import { createTheme, withStyles } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Login from "../Login/"
import Home from "../Home/"

const theme = createTheme({
    typography: {
        htmlFontSize: 16,
        fontFamily: "Roboto",
        fontSize: 14,
        button: {
            textTransform: "inherit"
        }
    }
});

const styles = {
    success: {
        color: `#000000 !important`,
        backgroundColor: "#ffffff !important"
    },
    error: {
        color: `red !important`,
        backgroundColor: "#ffffff !important"
    }
};

class Site extends Component {

    componentDidMount() {

    }


    render() {
        return (
            <ThemeProvider theme={theme}>
                <Router>
                    <Switch>
                        <Route exact path="/login" component={Login} />
                        <Route path="/" component={Home} />
                    </Switch>
                </Router>
            </ThemeProvider>
        )
    }
}
export default withStyles(styles)(Site)